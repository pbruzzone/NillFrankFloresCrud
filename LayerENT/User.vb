﻿Public Class User

    Public Property Id As Guid
    Public Property Name As String
    Public Property LastName As String
    Public Property UserName As String
    Public Property Password As String
    Public Property State As Boolean

    Public Overrides Function ToString() As String
        Return String.Format("{0} {1}", Name, LastName)
    End Function

End Class
