﻿Imports System.Runtime.Remoting.Messaging

Public Class Product

    Public Property Id As Guid
    Public Property RegistrationDate As Date
    Public Property Name As String
    Public Property Price As Decimal
    Public Property Quantity As Double
    Public Property State As Boolean

    Public Overrides Function ToString() As String
        Return Name
    End Function

End Class
