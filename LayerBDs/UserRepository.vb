﻿
Imports System.Configuration
Imports System.Data.SqlClient
Imports LayerDO.Abstract.Repositories

Imports LayerENT
Public Class UserRepository
    Implements IUserRepository

    Private ReadOnly _connString As String

    Public Sub New()
        _connString = ConfigurationManager.ConnectionStrings("Fernando").ConnectionString
    End Sub

    Public Sub New(connString As String)
        _connString = connString
    End Sub

    Public Function GetAll() As IEnumerable(Of User) Implements IUserRepository.GetAll
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("ListAllUsers")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            Using reader = cmd.ExecuteReader()
                Dim users = New List(Of User)
                While reader.Read()
                    Dim user = New User With {
                        .Id = reader.GetGuid(0),
                        .Name = reader.GetString(1),
                        .LastName = reader.GetString(2),
                        .UserName = reader.GetString(3),
                        .State = reader.GetBoolean(4)
                    }
                    users.Add(user)
                End While
                Return users
            End Using
        End Using
    End Function

    Private Function GetByUserName(userName As String) As User Implements IUserRepository.GetByUserName
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("InfoUser")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@usrName", userName)
            cmd.Connection = conn

            Using reader = cmd.ExecuteReader()
                If reader.Read() Then
                    Dim user = New User With {
                        .Id = reader.GetGuid(0),
                        .Name = reader.GetString(1),
                        .LastName = reader.GetString(2),
                        .UserName = reader.GetString(3)
                    }
                    Return user
                End If
            End Using
        End Using
    End Function

    Public Function ValidateCredentials(userName As String, password As String) As Boolean Implements IUserRepository.ValidateCredentials
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("isValidModel")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@usrName", userName)
            cmd.Parameters.AddWithValue("@usrPass", password)

            Using reader = cmd.ExecuteReader
                Return reader.HasRows
            End Using
        End Using
    End Function

    Public Sub Insert(entity As User) Implements IRepository(Of User).Insert
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("AddNewUser")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@identificador", entity.Id)
            cmd.Parameters.AddWithValue("@nombres", entity.Name)
            cmd.Parameters.AddWithValue("@apellidos", entity.LastName)
            cmd.Parameters.AddWithValue("@Usrname", entity.UserName)
            cmd.Parameters.AddWithValue("@UsePass", entity.Password)
            cmd.Parameters.AddWithValue("@State", entity.State)

            cmd.ExecuteNonQuery()
        End Using
    End Sub

    Public Sub Update(entity As User) Implements IUserRepository.Update
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("UpdateUser")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@identificador", entity.Id)
            cmd.Parameters.AddWithValue("@nombres", entity.Name)
            cmd.Parameters.AddWithValue("@apellidos", entity.LastName)
            cmd.Parameters.AddWithValue("@Usrname", entity.UserName)
            cmd.Parameters.AddWithValue("@UsePass", entity.Password)

            cmd.ExecuteNonQuery()

        End Using
    End Sub

    ''' <summary>
    ''' Marca como inactivo a un Usuario
    ''' </summary>
    ''' <param name="id">Identificador unico del Usuario</param>
    Public Sub Delete(id As Guid) Implements IRepository(Of User).Delete
        Using conn = New SqlConnection(_connString)
            conn.Open()
            Dim cmd = New SqlCommand("DeleteUser")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn

            cmd.Parameters.AddWithValue("@identificador", id)
            cmd.ExecuteNonQuery()
        End Using
    End Sub

End Class
