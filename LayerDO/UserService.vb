﻿
Imports LayerDO.Abstract.Repositories

Imports LayerENT
Public Class UserService

   Private ReadOnly _userRepository As IUserRepository

   Public Sub New(userRepository As IUserRepository)
       _userRepository = userRepository
   End Sub
   
    Public Function GetAll() As IEnumerable(Of User) 
       Return _userRepository.GetAll()
    End Function

    Public Function GetById(id As Guid) As User 
        Return GetAll().FirstOrDefault(Function(user) user.Id = id)
    End Function

    Public Function GetByUserName(userName As String) As User
       Return _userRepository.GetByUserName(userName)
    End Function

    Public Function ValidateCredentials(userName As String, password As String) As Boolean
        Return _userRepository.ValidateCredentials(userName, password)
    End Function

    Public Sub AddNew(entity As User) 
        _userRepository.Insert(entity)
    End Sub

    Public Sub Update(entity As User) 
        _userRepository.Update(entity)
    End Sub

    ''' <summary>
    ''' Marca como inactivo a un usuario
    ''' </summary>
    ''' <param name="entity">Usuario</param>
    Public Sub InactiveUser(entity As User) 
        entity.State = false
        _userRepository.Delete(entity.Id)
    End Sub

End Class
