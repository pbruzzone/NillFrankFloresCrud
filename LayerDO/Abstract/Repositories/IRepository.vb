﻿Namespace Abstract.Repositories
    Public Interface IRepository(Of T)

        Function GetAll() As IEnumerable(Of T)
        Sub Insert(entity As T)  
        Sub Update(entity As T)
        Sub Delete(id As Guid)

    End Interface
End NameSpace