﻿Imports LayerBDs
Imports LayerDO
Imports LayerPresentation.My.Resources

Public Class LoginForm

    Private ReadOnly _userService As UserService = New UserService(New UserRepository())

    Private Sub LoginForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetTime()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        SetTime()
    End Sub

    Private Sub SetTime()
        timeLabel.Text = Date.Now.ToLongTimeString()
    End Sub

    Private Sub loginButton_Click(sender As Object, e As EventArgs) Handles loginButton.Click
        If CredentialsEntered() Then
            If _userService.ValidateCredentials(userNameTextBox.Text, passwordTextBox.Text) = True Then
                Dim user = _userService.GetByUserName(userNameTextBox.Text.Trim)
                Dim msg = String.Format(Bienvenida, user.UserName, user.Name, user.LastName)
                MessageBox.Show(msg, AccesoConcedido, MessageBoxButtons.OK, MessageBoxIcon.Information)
                SetUserInfo()
                DialogResult = DialogResult.OK
            Else
                MessageBox.Show(CredencialesNoValidas,ReviseCredencialesTitle,
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                userNameTextBox.Clear()
                passwordTextBox.Clear()
                userNameTextBox.Focus()
            End If
        Else
            MsgBox(IngreseCredenciales, MsgBoxStyle.Information, Title:=SinCredencialesTitle)
            userNameTextBox.Focus()
        End If
    End Sub

    Private Function CredentialsEntered() As Boolean
        Return userNameTextBox.Text <> String.Empty And passwordTextBox.Text <> String.Empty
    End Function

    Public Sub SetUserInfo()
        Dim user = _userService.GetByUserName(userNameTextBox.Text.Trim)

        If user IsNot Nothing Then
            MainForm.UserName = user.UserName
            MainForm.UserFullName = String.Format("{0} {1}", user.Name, user.LastName)
        End If
    End Sub

    Private Sub loginCancelButton_Click(sender As Object, e As EventArgs) Handles loginCancelButton.Click
        Close()
    End Sub
End Class