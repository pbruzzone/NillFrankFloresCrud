﻿Imports LayerBDs
Imports LayerDO
Imports LayerPresentation.My.Resources

Namespace Users

    Public Class UserListForm

        Private ReadOnly _userService As New UserService(New UserRepository())

        Private Sub UserListForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            usersGrid.AutoGenerateColumns = False
            LoadUsers()
        End Sub

        Public Sub LoadUsers()

            usersGrid.DataSource = _userService.GetAll().ToList()

            SetControls(usersGrid.RowCount > 0)

            recordsLabel.Text = String.Format(NumeroDeRegistrosEncontrados, usersGrid.Rows.Count)

        End Sub

        Private Sub SetControls(active As Boolean)
            filterGroupBox.Enabled = active
            emptyLinkLabel.Visible = Not active
        End Sub

        Public Sub Filteringproducts()
            'TODO: ver si se puede mejorar en un futuro refactoreo
            Select Case propertyNameCombo.Text.ToUpper()
                Case "NOMBRES"
                     usersGrid.DataSource = _userService.GetAll() _
                        .Where(Function(u) u.Name.ToUpper() _
                        .Contains(filterTextBox.Text.Trim().ToUpper())).ToList()
                Case "APELLIDOS"
                    usersGrid.DataSource = _userService.GetAll() _
                        .Where(Function(u) u.LastName.ToUpper() _
                        .Contains(filterTextBox.Text.Trim().ToUpper())).ToList()
                Case "NOMBREUSUARIO"
                    usersGrid.DataSource = _userService.GetAll() _
                        .Where(Function(u) u.UserName.ToUpper() _
                        .Contains(filterTextBox.Text.Trim().ToUpper())).ToList()
            End Select

            emptyLinkLabel.Visible = usersGrid.RowCount = 0
        End Sub

        Private Sub filterTextBox_TextChanged(sender As Object, e As EventArgs) Handles filterTextBox.TextChanged
            Filteringproducts()
        End Sub
    End Class
End Namespace