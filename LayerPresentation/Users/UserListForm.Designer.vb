﻿Namespace Users
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class UserListForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.filterGroupBox = New System.Windows.Forms.GroupBox()
        Me.filterTextBox = New System.Windows.Forms.TextBox()
        Me.propertyNameCombo = New System.Windows.Forms.ComboBox()
        Me.emptyLinkLabel = New System.Windows.Forms.LinkLabel()
        Me.recordsLabel = New System.Windows.Forms.Label()
        Me.usersGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnUserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserLastName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserUserName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnUserState = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.filterGroupBox.SuspendLayout
        CType(Me.usersGrid,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'filterGroupBox
        '
        Me.filterGroupBox.Controls.Add(Me.filterTextBox)
        Me.filterGroupBox.Controls.Add(Me.propertyNameCombo)
        Me.filterGroupBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.filterGroupBox.Location = New System.Drawing.Point(6, 7)
        Me.filterGroupBox.Name = "filterGroupBox"
        Me.filterGroupBox.Size = New System.Drawing.Size(419, 59)
        Me.filterGroupBox.TabIndex = 4
        Me.filterGroupBox.TabStop = false
        Me.filterGroupBox.Text = "Filtrado Y Busqueda de Usuarios"
        '
        'filterTextBox
        '
        Me.filterTextBox.Location = New System.Drawing.Point(172, 22)
        Me.filterTextBox.Name = "filterTextBox"
        Me.filterTextBox.Size = New System.Drawing.Size(242, 22)
        Me.filterTextBox.TabIndex = 0
        '
        'propertyNameCombo
        '
        Me.propertyNameCombo.FormattingEnabled = true
        Me.propertyNameCombo.Items.AddRange(New Object() {"Nombres", "Apellidos", "NombreUsuario"})
        Me.propertyNameCombo.Location = New System.Drawing.Point(6, 23)
        Me.propertyNameCombo.Name = "propertyNameCombo"
        Me.propertyNameCombo.Size = New System.Drawing.Size(160, 21)
        Me.propertyNameCombo.TabIndex = 1
        Me.propertyNameCombo.Text = "Nombres"
        '
        'emptyLinkLabel
        '
        Me.emptyLinkLabel.AutoSize = true
        Me.emptyLinkLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.emptyLinkLabel.Location = New System.Drawing.Point(60, 163)
        Me.emptyLinkLabel.Name = "emptyLinkLabel"
        Me.emptyLinkLabel.Size = New System.Drawing.Size(342, 18)
        Me.emptyLinkLabel.TabIndex = 7
        Me.emptyLinkLabel.TabStop = true
        Me.emptyLinkLabel.Text = "Lo Sentimos Pero Lista No Contiene Items Para Mostrar"
        '
        'recordsLabel
        '
        Me.recordsLabel.AutoSize = true
        Me.recordsLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.recordsLabel.Location = New System.Drawing.Point(104, 258)
        Me.recordsLabel.Name = "recordsLabel"
        Me.recordsLabel.Size = New System.Drawing.Size(10, 13)
        Me.recordsLabel.TabIndex = 6
        Me.recordsLabel.Text = "I"
        '
        'usersGrid
        '
        Me.usersGrid.AllowUserToAddRows = false
        Me.usersGrid.AllowUserToDeleteRows = false
        Me.usersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.usersGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnUserName, Me.ColumnUserLastName, Me.ColumnUserUserName, Me.ColumnUserState})
        Me.usersGrid.Location = New System.Drawing.Point(2, 72)
        Me.usersGrid.Name = "usersGrid"
        Me.usersGrid.ReadOnly = true
        Me.usersGrid.RowHeadersVisible = false
        Me.usersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.usersGrid.Size = New System.Drawing.Size(452, 183)
        Me.usersGrid.TabIndex = 5
        '
        'ColumnUserName
        '
        Me.ColumnUserName.DataPropertyName = "Name"
        Me.ColumnUserName.HeaderText = "Nombres"
        Me.ColumnUserName.Name = "ColumnUserName"
        Me.ColumnUserName.ReadOnly = true
        Me.ColumnUserName.Width = 150
        '
        'ColumnUserLastName
        '
        Me.ColumnUserLastName.DataPropertyName = "LastName"
        Me.ColumnUserLastName.HeaderText = "Apellidos"
        Me.ColumnUserLastName.Name = "ColumnUserLastName"
        Me.ColumnUserLastName.ReadOnly = true
        Me.ColumnUserLastName.Width = 150
        '
        'ColumnUserUserName
        '
        Me.ColumnUserUserName.DataPropertyName = "UserName"
        Me.ColumnUserUserName.HeaderText = "NombreUsuario"
        Me.ColumnUserUserName.Name = "ColumnUserUserName"
        Me.ColumnUserUserName.ReadOnly = true
        Me.ColumnUserUserName.Width = 95
        '
        'ColumnUserState
        '
        Me.ColumnUserState.DataPropertyName = "State"
        Me.ColumnUserState.HeaderText = "Activo"
        Me.ColumnUserState.Name = "ColumnUserState"
        Me.ColumnUserState.ReadOnly = true
        Me.ColumnUserState.Width = 50
        '
        'UserListForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 275)
        Me.Controls.Add(Me.filterGroupBox)
        Me.Controls.Add(Me.emptyLinkLabel)
        Me.Controls.Add(Me.recordsLabel)
        Me.Controls.Add(Me.usersGrid)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "UserListForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lista de Usuarios"
        Me.filterGroupBox.ResumeLayout(false)
        Me.filterGroupBox.PerformLayout
        CType(Me.usersGrid,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

        Friend WithEvents filterGroupBox As GroupBox
        Friend WithEvents filterTextBox As TextBox
        Friend WithEvents propertyNameCombo As ComboBox
        Friend WithEvents emptyLinkLabel As LinkLabel
        Friend WithEvents recordsLabel As Label
        Friend WithEvents usersGrid As DataGridView
        Friend WithEvents ColumnUserName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserLastName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserUserName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnUserState As DataGridViewCheckBoxColumn
    End Class
End NameSpace