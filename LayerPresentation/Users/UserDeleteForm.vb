﻿Imports LayerBDs
Imports LayerDO
Imports LayerENT
Imports LayerPresentation.My.Resources
Namespace Users

    Public Class UserDeleteForm

        Private ReadOnly _userService As New UserService(New UserRepository())

        Private Sub UserDeleteForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            usersGrid.AutoGenerateColumns = False
            LoadUsers()
        End Sub
        Public Sub LoadUsers()
            usersGrid.DataSource = _userService.GetAll().ToList()
        End Sub

        Private Sub usersGrid_SelectionChanged(sender As Object, e As EventArgs) Handles usersGrid.SelectionChanged
            ShowItem()
        End Sub

        Public Sub ShowItem()
            If usersGrid.SelectedCells.Count = 0 Then Return
            userGroupBox.Text = SeleccionUsuarioTitle
            nameTextBox.Text = usersGrid.SelectedCells.Item(1).Value
            lastNameTextBox.Text = usersGrid.SelectedCells.Item(2).Value
            userNameTextBox.Text = usersGrid.SelectedCells.Item(3).Value
        End Sub

        Private Sub deleteButton_Click(sender As Object, e As EventArgs) Handles deleteButton.Click

            Dim result = MessageBox.Show(DeseaEliminarRegistro, Application.CompanyName,
                                         MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

            If result = DialogResult.OK Then ToggleDeletionMode()

        End Sub

        Public Sub ToggleDeletionMode()
            deleteButton.Enabled = Not deleteButton.Enabled
            deleteAcceptButton.Enabled = Not deleteAcceptButton.Enabled
            deleteCancelButton.Enabled = Not deleteCancelButton.Enabled
            usersGrid.Enabled = Not usersGrid.Enabled
        End Sub

        Private Sub deleteAcceptButton_Click(sender As Object, e As EventArgs) Handles deleteAcceptButton.Click
            Try
                Dim usuario = DirectCast(usersGrid.SelectedRows(0).DataBoundItem, User)

                _userService.InactiveUser(usuario)
                LoadUsers()

                MsgBox(EliminadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoEliminar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try
        End Sub

        Private Sub deleteCancelButton_Click(sender As Object, e As EventArgs) Handles deleteCancelButton.Click
            ToggleDeletionMode()
        End Sub

    End Class
End Namespace