﻿Namespace Products
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductListForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.filterGroupBox = New System.Windows.Forms.GroupBox()
        Me.filterTextBox = New System.Windows.Forms.TextBox()
        Me.propertyNameCombo = New System.Windows.Forms.ComboBox()
        Me.productsGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnProductId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductRegistrationDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductState = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.recordsLabel = New System.Windows.Forms.Label()
        Me.emptyLinkLabel = New System.Windows.Forms.LinkLabel()
        Me.filterGroupBox.SuspendLayout
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'filterGroupBox
        '
        Me.filterGroupBox.Controls.Add(Me.filterTextBox)
        Me.filterGroupBox.Controls.Add(Me.propertyNameCombo)
        Me.filterGroupBox.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.filterGroupBox.Location = New System.Drawing.Point(8, 9)
        Me.filterGroupBox.Name = "filterGroupBox"
        Me.filterGroupBox.Size = New System.Drawing.Size(536, 59)
        Me.filterGroupBox.TabIndex = 0
        Me.filterGroupBox.TabStop = false
        Me.filterGroupBox.Text = "Filtrado Y Busqueda de Productos"
        '
        'filterTextBox
        '
        Me.filterTextBox.Location = New System.Drawing.Point(172, 22)
        Me.filterTextBox.Name = "filterTextBox"
        Me.filterTextBox.Size = New System.Drawing.Size(358, 22)
        Me.filterTextBox.TabIndex = 0
        '
        'propertyNameCombo
        '
        Me.propertyNameCombo.FormattingEnabled = true
        Me.propertyNameCombo.Items.AddRange(New Object() {"Precio", "Stock", "Fecha Registro"})
        Me.propertyNameCombo.Location = New System.Drawing.Point(6, 23)
        Me.propertyNameCombo.Name = "propertyNameCombo"
        Me.propertyNameCombo.Size = New System.Drawing.Size(160, 21)
        Me.propertyNameCombo.TabIndex = 1
        Me.propertyNameCombo.Text = "Descripcion"
        '
        'productsGrid
        '
        Me.productsGrid.AllowUserToAddRows = false
        Me.productsGrid.AllowUserToDeleteRows = false
        Me.productsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.productsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnProductId, Me.ColumnProductDescription, Me.ColumnProductRegistrationDate, Me.ColumnProductPrice, Me.ColumnProductQuantity, Me.ColumnProductState})
        Me.productsGrid.Location = New System.Drawing.Point(4, 74)
        Me.productsGrid.Name = "productsGrid"
        Me.productsGrid.ReadOnly = true
        Me.productsGrid.RowHeadersVisible = false
        Me.productsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.productsGrid.Size = New System.Drawing.Size(790, 183)
        Me.productsGrid.TabIndex = 1
        '
        'ColumnProductId
        '
        Me.ColumnProductId.HeaderText = "Id"
        Me.ColumnProductId.Name = "ColumnProductId"
        Me.ColumnProductId.ReadOnly = true
        Me.ColumnProductId.Visible = false
        '
        'ColumnProductDescription
        '
        Me.ColumnProductDescription.DataPropertyName = "Name"
        Me.ColumnProductDescription.HeaderText = "Descripcion"
        Me.ColumnProductDescription.Name = "ColumnProductDescription"
        Me.ColumnProductDescription.ReadOnly = true
        Me.ColumnProductDescription.Width = 515
        '
        'ColumnProductRegistrationDate
        '
        Me.ColumnProductRegistrationDate.DataPropertyName = "RegistrationDate"
        Me.ColumnProductRegistrationDate.HeaderText = "Fecha Registrado"
        Me.ColumnProductRegistrationDate.Name = "ColumnProductRegistrationDate"
        Me.ColumnProductRegistrationDate.ReadOnly = true
        Me.ColumnProductRegistrationDate.Width = 120
        '
        'ColumnProductPrice
        '
        Me.ColumnProductPrice.DataPropertyName = "Price"
        Me.ColumnProductPrice.HeaderText = "Precio"
        Me.ColumnProductPrice.Name = "ColumnProductPrice"
        Me.ColumnProductPrice.ReadOnly = true
        Me.ColumnProductPrice.Width = 50
        '
        'ColumnProductQuantity
        '
        Me.ColumnProductQuantity.DataPropertyName = "Quantity"
        Me.ColumnProductQuantity.HeaderText = "Cantidad"
        Me.ColumnProductQuantity.Name = "ColumnProductQuantity"
        Me.ColumnProductQuantity.ReadOnly = true
        Me.ColumnProductQuantity.Width = 50
        '
        'ColumnProductState
        '
        Me.ColumnProductState.DataPropertyName = "State"
        Me.ColumnProductState.HeaderText = "Activo"
        Me.ColumnProductState.Name = "ColumnProductState"
        Me.ColumnProductState.ReadOnly = true
        Me.ColumnProductState.Width = 50
        '
        'recordsLabel
        '
        Me.recordsLabel.AutoSize = true
        Me.recordsLabel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.recordsLabel.Location = New System.Drawing.Point(484, 257)
        Me.recordsLabel.Name = "recordsLabel"
        Me.recordsLabel.Size = New System.Drawing.Size(10, 13)
        Me.recordsLabel.TabIndex = 2
        Me.recordsLabel.Text = "I"
        '
        'emptyLinkLabel
        '
        Me.emptyLinkLabel.AutoSize = true
        Me.emptyLinkLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.emptyLinkLabel.Location = New System.Drawing.Point(228, 140)
        Me.emptyLinkLabel.Name = "emptyLinkLabel"
        Me.emptyLinkLabel.Size = New System.Drawing.Size(342, 18)
        Me.emptyLinkLabel.TabIndex = 3
        Me.emptyLinkLabel.TabStop = true
        Me.emptyLinkLabel.Text = "Lo Sentimos Pero Lista No Contiene Items Para Mostrar"
        '
        'ProductListForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(798, 276)
        Me.Controls.Add(Me.emptyLinkLabel)
        Me.Controls.Add(Me.recordsLabel)
        Me.Controls.Add(Me.productsGrid)
        Me.Controls.Add(Me.filterGroupBox)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "ProductListForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Product List"
        Me.filterGroupBox.ResumeLayout(false)
        Me.filterGroupBox.PerformLayout
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

        Friend WithEvents filterGroupBox As GroupBox
        Friend WithEvents filterTextBox As TextBox
        Friend WithEvents propertyNameCombo As ComboBox
        Friend WithEvents productsGrid As DataGridView
        Friend WithEvents recordsLabel As Label
        Friend WithEvents emptyLinkLabel As LinkLabel
        Friend WithEvents ColumnProductId As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductDescription As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductRegistrationDate As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductPrice As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductQuantity As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductState As DataGridViewCheckBoxColumn
    End Class
End NameSpace