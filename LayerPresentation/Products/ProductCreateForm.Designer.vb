﻿Namespace Products
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductCreateForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductCreateForm))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.quantityTextBox = New System.Windows.Forms.TextBox()
        Me.priceTextBox = New System.Windows.Forms.TextBox()
        Me.dateLabel = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.descriptionTextBox = New System.Windows.Forms.TextBox()
        Me.idTextBox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.insertButton = New System.Windows.Forms.Button()
        Me.insertCancelButton = New System.Windows.Forms.Button()
        Me.errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.stateChackBox = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.stateChackBox)
        Me.GroupBox1.Controls.Add(Me.quantityTextBox)
        Me.GroupBox1.Controls.Add(Me.priceTextBox)
        Me.GroupBox1.Controls.Add(Me.dateLabel)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.descriptionTextBox)
        Me.GroupBox1.Controls.Add(Me.idTextBox)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 4)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(474, 223)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Registrar un Nuevo Producto"
        '
        'quantityTextBox
        '
        Me.quantityTextBox.Location = New System.Drawing.Point(275, 138)
        Me.quantityTextBox.Name = "quantityTextBox"
        Me.quantityTextBox.Size = New System.Drawing.Size(100, 20)
        Me.quantityTextBox.TabIndex = 10
        '
        'priceTextBox
        '
        Me.priceTextBox.Location = New System.Drawing.Point(275, 90)
        Me.priceTextBox.Name = "priceTextBox"
        Me.priceTextBox.Size = New System.Drawing.Size(100, 20)
        Me.priceTextBox.TabIndex = 9
        '
        'dateLabel
        '
        Me.dateLabel.AutoSize = true
        Me.dateLabel.Location = New System.Drawing.Point(275, 48)
        Me.dateLabel.Name = "dateLabel"
        Me.dateLabel.Size = New System.Drawing.Size(12, 16)
        Me.dateLabel.TabIndex = 8
        Me.dateLabel.Text = "I"
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Location = New System.Drawing.Point(275, 161)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Estado"
        '
        'descriptionTextBox
        '
        Me.descriptionTextBox.Location = New System.Drawing.Point(7, 90)
        Me.descriptionTextBox.Multiline = true
        Me.descriptionTextBox.Name = "descriptionTextBox"
        Me.descriptionTextBox.Size = New System.Drawing.Size(250, 118)
        Me.descriptionTextBox.TabIndex = 6
        '
        'idTextBox
        '
        Me.idTextBox.Enabled = false
        Me.idTextBox.Location = New System.Drawing.Point(7, 45)
        Me.idTextBox.Name = "idTextBox"
        Me.idTextBox.ReadOnly = true
        Me.idTextBox.Size = New System.Drawing.Size(250, 20)
        Me.idTextBox.TabIndex = 5
        Me.idTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(275, 122)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Cantidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(275, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Precio"
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(275, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "F. de Registro"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(7, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripcion"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(7, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo"
        '
        'insertButton
        '
        Me.insertButton.Font = New System.Drawing.Font("Trebuchet MS", 10!)
        Me.insertButton.Image = CType(resources.GetObject("insertButton.Image"),System.Drawing.Image)
        Me.insertButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.insertButton.Location = New System.Drawing.Point(120, 234)
        Me.insertButton.Name = "insertButton"
        Me.insertButton.Size = New System.Drawing.Size(122, 50)
        Me.insertButton.TabIndex = 1
        Me.insertButton.Text = "&Registrar"
        Me.insertButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.insertButton.UseVisualStyleBackColor = true
        '
        'insertCancelButton
        '
        Me.insertCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.insertCancelButton.Font = New System.Drawing.Font("Trebuchet MS", 10!)
        Me.insertCancelButton.Image = CType(resources.GetObject("insertCancelButton.Image"),System.Drawing.Image)
        Me.insertCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.insertCancelButton.Location = New System.Drawing.Point(242, 234)
        Me.insertCancelButton.Name = "insertCancelButton"
        Me.insertCancelButton.Size = New System.Drawing.Size(122, 50)
        Me.insertCancelButton.TabIndex = 2
        Me.insertCancelButton.Text = "&Cancelar"
        Me.insertCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.insertCancelButton.UseVisualStyleBackColor = true
        '
        'errorProvider
        '
        Me.errorProvider.ContainerControl = Me
        '
        'stateChackBox
        '
        Me.stateChackBox.AutoSize = true
        Me.stateChackBox.Checked = true
        Me.stateChackBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.stateChackBox.Location = New System.Drawing.Point(275, 190)
        Me.stateChackBox.Name = "stateChackBox"
        Me.stateChackBox.Size = New System.Drawing.Size(59, 20)
        Me.stateChackBox.TabIndex = 11
        Me.stateChackBox.Text = "Activo"
        Me.stateChackBox.UseVisualStyleBackColor = true
        '
        'ProductCreateForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 16!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.CancelButton = Me.insertCancelButton
        Me.ClientSize = New System.Drawing.Size(484, 293)
        Me.Controls.Add(Me.insertCancelButton)
        Me.Controls.Add(Me.insertButton)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "ProductCreateForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Products Create"
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub

        Friend WithEvents GroupBox1 As GroupBox
        Friend WithEvents Label1 As Label
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents Label6 As Label
        Friend WithEvents Label5 As Label
        Friend WithEvents Label4 As Label
        Friend WithEvents quantityTextBox As TextBox
        Friend WithEvents priceTextBox As TextBox
        Friend WithEvents dateLabel As Label
        Friend WithEvents descriptionTextBox As TextBox
        Friend WithEvents idTextBox As TextBox
        Friend WithEvents insertButton As Button
        Friend WithEvents insertCancelButton As Button
        Friend WithEvents errorProvider As ErrorProvider
        Friend WithEvents stateChackBox As CheckBox
    End Class
End NameSpace