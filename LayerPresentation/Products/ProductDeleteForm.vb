﻿Imports LayerBDs
Imports LayerDO
Imports LayerENT
Imports LayerPresentation.My.Resources

Namespace Products

    Public Class ProductDeleteForm

        Private ReadOnly _productService As New ProductService(New ProductRepository())

        Private Sub ProductDeleteForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            productsGrid.AutoGenerateColumns = False
            LoadProducts()
        End Sub

        Public Sub LoadProducts()

            productsGrid.DataSource = _productService.GetAll().ToList()
            productsGrid.ColumnHeadersVisible = productsGrid.RowCount > 0

        End Sub

        Private Sub deleteButton_Click(sender As Object, e As EventArgs) Handles deleteButton.Click

            Dim result = MessageBox.Show(DeseaEliminarRegistro, Application.CompanyName,
                                   MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

            If result = DialogResult.OK Then ToggleDeletionMode()

        End Sub

        Private Sub ToggleDeletionMode()
            deleteAcceptButton.Enabled = Not deleteAcceptButton.Enabled
            deleteCancelButton.Enabled = Not deleteCancelButton.Enabled
            deleteButton.Enabled = Not deleteButton.Enabled
            productsGrid.Enabled = Not productsGrid.Enabled
        End Sub

        Private Sub deleteCancelButton_Click(sender As Object, e As EventArgs) Handles deleteCancelButton.Click
            ToggleDeletionMode()
        End Sub

        Private Sub deleteAcceptButton_Click(sender As Object, e As EventArgs) Handles deleteAcceptButton.Click
            Try

                Dim prod = DirectCast(productsGrid.SelectedRows(0).DataBoundItem, Product)

                _productService.InactiveProduct(prod)

                ClearControls()
                ToggleDeletionMode()
                LoadProducts()

                MsgBox(EliminadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoEliminar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try
        End Sub

        Private Sub ClearControls()
            productGroupBox.Text = String.Empty
            descriptionTextBox.Clear()
            priceTextBox.Clear()
            quantityTextBox.Clear()
        End Sub

        Private Sub productsGrid_SelectionChanged(sender As Object, e As EventArgs) Handles productsGrid.SelectionChanged
            ShowItem()
        End Sub
    
        Public Sub ShowItem()
            If productsGrid.SelectedRows.Count = 0 Then Return
            productGroupBox.Text = SeleccionProductoTitle
            descriptionTextBox.Text = productsGrid.SelectedCells.Item(1).Value
            priceTextBox.Text = productsGrid.SelectedCells.Item(3).Value
            quantityTextBox.Text = productsGrid.SelectedCells.Item(4).Value
        End Sub

    End Class
End Namespace