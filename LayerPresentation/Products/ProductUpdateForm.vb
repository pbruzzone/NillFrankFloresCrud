﻿Imports System.ComponentModel
Imports LayerBDs
Imports LayerDO
Imports LayerPresentation.My.Resources

Namespace Products

    Public Class ProductUpdateForm

        Private ReadOnly _productService As New ProductService(New ProductRepository())

        Private Sub ProductUpdateForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            productsGrid.AutoGenerateColumns = False
            LoadProducts()
        End Sub

        Public Sub LoadProducts()
            productsGrid.DataSource = _productService.GetAll().ToList()
        End Sub

        Private Sub editButton_Click(sender As Object, e As EventArgs) Handles editButton.Click

            Dim result = MessageBox.Show(DeseaEditarRegistro, Application.CompanyName,
                                   MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

            If result = DialogResult.OK Then ToggleEditionMode()

        End Sub

        Private Sub ToggleEditionMode()
            descriptionTextBox.Enabled = Not descriptionTextBox.Enabled
            priceTextBox.Enabled = Not priceTextBox.Enabled
            quantityTextBox.Enabled = Not quantityTextBox.Enabled
            editAcceptButton.Enabled = Not editAcceptButton.Enabled
            editCancelButton.Enabled = Not editCancelButton.Enabled
            editButton.Enabled = Not editButton.Enabled
            productsGrid.Enabled = Not productsGrid.Enabled
        End Sub

        Private Sub editAcceptButton_Click(sender As Object, e As EventArgs) Handles editAcceptButton.Click
            If Not ValidateChildren() Then Return
            Try
                Dim product = DirectCast(productsGrid.SelectedRows.Item(0).DataBoundItem, LayerENT.Product)
                product.Name = descriptionTextBox.Text.Trim()
                product.Price() = priceTextBox.Text.Trim()
                product.Quantity = quantityTextBox.Text.Trim()

                _productService.Update(product)

                LoadProducts()
                ToggleEditionMode()

                MsgBox(ActualizadoConExito, MsgBoxStyle.Information, Title:=Application.CompanyName)

            Catch ex As Exception
                MsgBox(NoSePudoActualizar, MsgBoxStyle.Critical, Title:=ErrorTitle)
            End Try
        End Sub

        Private Sub editCancelButton_Click(sender As Object, e As EventArgs) Handles editCancelButton.Click
            ToggleEditionMode()
            ShowItem()
            ValidateChildren()
        End Sub

        Private Sub Required_Validating(sender As Object, e As CancelEventArgs) Handles descriptionTextBox.Validating, priceTextBox.Validating, quantityTextBox.Validating
            Dim control = DirectCast(sender, TextBox)
            If control.Text.Length > 0 Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, CampoRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub Number_Validating(sender As Object, e As CancelEventArgs) Handles quantityTextBox.Validating, priceTextBox.Validating
            Dim control = DirectCast(sender, Control)
            If IsNumeric(control.Text) Then
                errorProvider.SetError(control, String.Empty)
            Else
                errorProvider.SetError(control, ErrorNumeroRequerido)
                e.Cancel = True
            End If
        End Sub

        Private Sub productsGrid_SelectionChanged(sender As Object, e As EventArgs) Handles productsGrid.SelectionChanged
            ShowItem()
        End Sub

        Private Sub ShowItem()
            If productsGrid.SelectedCells.Count = 0 Then Return
            productGroupBox.Text = SeleccionProductoTitle
            descriptionTextBox.Text = productsGrid.SelectedCells(1).Value
            priceTextBox.Text = productsGrid.SelectedCells(3).Value
            quantityTextBox.Text = productsGrid.SelectedCells(4).Value
        End Sub

    End Class
End Namespace