﻿Namespace Stock
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DiscountStockForm
        Inherits System.Windows.Forms.Form

        'Form reemplaza a Dispose para limpiar la lista de componentes.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Requerido por el Diseñador de Windows Forms
        Private components As System.ComponentModel.IContainer

        'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
        'Se puede modificar usando el Diseñador de Windows Forms.  
        'No lo modifique con el editor de código.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DiscountStockForm))
        Me.emptyLinkLabel = New System.Windows.Forms.LinkLabel()
        Me.productsGrid = New System.Windows.Forms.DataGridView()
        Me.ColumnProductName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductRegistrationDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnProductState = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.productGroupBox = New System.Windows.Forms.GroupBox()
        Me.discountTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.quantityTextBox = New System.Windows.Forms.TextBox()
        Me.priceTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.descriptionTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.outProductButton = New System.Windows.Forms.Button()
        Me.outCancelButton = New System.Windows.Forms.Button()
        Me.errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).BeginInit
        Me.productGroupBox.SuspendLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'emptyLinkLabel
        '
        Me.emptyLinkLabel.AutoSize = true
        Me.emptyLinkLabel.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.emptyLinkLabel.Location = New System.Drawing.Point(164, 80)
        Me.emptyLinkLabel.Name = "emptyLinkLabel"
        Me.emptyLinkLabel.Size = New System.Drawing.Size(342, 18)
        Me.emptyLinkLabel.TabIndex = 5
        Me.emptyLinkLabel.TabStop = true
        Me.emptyLinkLabel.Text = "Lo Sentimos Pero Lista No Contiene Items Para Mostrar"
        '
        'productsGrid
        '
        Me.productsGrid.AllowUserToAddRows = false
        Me.productsGrid.AllowUserToDeleteRows = false
        Me.productsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.productsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnProductName, Me.ColumnProductRegistrationDate, Me.ColumnProductPrice, Me.ColumnProductQuantity, Me.ColumnProductState})
        Me.productsGrid.Location = New System.Drawing.Point(3, 5)
        Me.productsGrid.Name = "productsGrid"
        Me.productsGrid.ReadOnly = true
        Me.productsGrid.RowHeadersVisible = false
        Me.productsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.productsGrid.Size = New System.Drawing.Size(664, 183)
        Me.productsGrid.TabIndex = 4
        '
        'ColumnProductName
        '
        Me.ColumnProductName.DataPropertyName = "Name"
        Me.ColumnProductName.HeaderText = "Descripcion"
        Me.ColumnProductName.Name = "ColumnProductName"
        Me.ColumnProductName.ReadOnly = true
        Me.ColumnProductName.Width = 393
        '
        'ColumnProductRegistrationDate
        '
        Me.ColumnProductRegistrationDate.DataPropertyName = "RegistrationDate"
        Me.ColumnProductRegistrationDate.HeaderText = "Fecha Registrado"
        Me.ColumnProductRegistrationDate.Name = "ColumnProductRegistrationDate"
        Me.ColumnProductRegistrationDate.ReadOnly = true
        Me.ColumnProductRegistrationDate.Width = 122
        '
        'ColumnProductPrice
        '
        Me.ColumnProductPrice.DataPropertyName = "Price"
        Me.ColumnProductPrice.HeaderText = "Precio"
        Me.ColumnProductPrice.Name = "ColumnProductPrice"
        Me.ColumnProductPrice.ReadOnly = true
        Me.ColumnProductPrice.Width = 50
        '
        'ColumnProductQuantity
        '
        Me.ColumnProductQuantity.DataPropertyName = "Quantity"
        Me.ColumnProductQuantity.HeaderText = "Cantidad"
        Me.ColumnProductQuantity.Name = "ColumnProductQuantity"
        Me.ColumnProductQuantity.ReadOnly = true
        Me.ColumnProductQuantity.Width = 50
        '
        'ColumnProductState
        '
        Me.ColumnProductState.DataPropertyName = "State"
        Me.ColumnProductState.HeaderText = "Activo"
        Me.ColumnProductState.Name = "ColumnProductState"
        Me.ColumnProductState.ReadOnly = true
        Me.ColumnProductState.Width = 50
        '
        'productGroupBox
        '
        Me.productGroupBox.Controls.Add(Me.discountTextBox)
        Me.productGroupBox.Controls.Add(Me.Label4)
        Me.productGroupBox.Controls.Add(Me.quantityTextBox)
        Me.productGroupBox.Controls.Add(Me.priceTextBox)
        Me.productGroupBox.Controls.Add(Me.Label3)
        Me.productGroupBox.Controls.Add(Me.Label2)
        Me.productGroupBox.Controls.Add(Me.descriptionTextBox)
        Me.productGroupBox.Controls.Add(Me.Label1)
        Me.productGroupBox.Location = New System.Drawing.Point(3, 192)
        Me.productGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productGroupBox.Name = "productGroupBox"
        Me.productGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.productGroupBox.Size = New System.Drawing.Size(516, 115)
        Me.productGroupBox.TabIndex = 6
        Me.productGroupBox.TabStop = false
        '
        'discountTextBox
        '
        Me.discountTextBox.Font = New System.Drawing.Font("Trebuchet MS", 20!, System.Drawing.FontStyle.Bold)
        Me.discountTextBox.Location = New System.Drawing.Point(379, 47)
        Me.discountTextBox.Multiline = true
        Me.discountTextBox.Name = "discountTextBox"
        Me.discountTextBox.Size = New System.Drawing.Size(120, 48)
        Me.discountTextBox.TabIndex = 9
        Me.discountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(408, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Descontar"
        '
        'quantityTextBox
        '
        Me.quantityTextBox.Enabled = false
        Me.quantityTextBox.Location = New System.Drawing.Point(311, 84)
        Me.quantityTextBox.Name = "quantityTextBox"
        Me.quantityTextBox.Size = New System.Drawing.Size(56, 20)
        Me.quantityTextBox.TabIndex = 7
        '
        'priceTextBox
        '
        Me.priceTextBox.Enabled = false
        Me.priceTextBox.Location = New System.Drawing.Point(311, 33)
        Me.priceTextBox.Name = "priceTextBox"
        Me.priceTextBox.Size = New System.Drawing.Size(56, 20)
        Me.priceTextBox.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(311, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cantidad"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(311, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Precio"
        '
        'descriptionTextBox
        '
        Me.descriptionTextBox.Enabled = false
        Me.descriptionTextBox.Location = New System.Drawing.Point(6, 33)
        Me.descriptionTextBox.Multiline = true
        Me.descriptionTextBox.Name = "descriptionTextBox"
        Me.descriptionTextBox.Size = New System.Drawing.Size(300, 71)
        Me.descriptionTextBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Descripcion"
        '
        'outProductButton
        '
        Me.outProductButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.outProductButton.Image = CType(resources.GetObject("outProductButton.Image"),System.Drawing.Image)
        Me.outProductButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.outProductButton.Location = New System.Drawing.Point(525, 204)
        Me.outProductButton.Name = "outProductButton"
        Me.outProductButton.Size = New System.Drawing.Size(142, 50)
        Me.outProductButton.TabIndex = 7
        Me.outProductButton.Text = "Registrar Salida"
        Me.outProductButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.outProductButton.UseVisualStyleBackColor = true
        '
        'outCancelButton
        '
        Me.outCancelButton.Font = New System.Drawing.Font("Trebuchet MS", 8.25!)
        Me.outCancelButton.Image = CType(resources.GetObject("outCancelButton.Image"),System.Drawing.Image)
        Me.outCancelButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.outCancelButton.Location = New System.Drawing.Point(525, 254)
        Me.outCancelButton.Name = "outCancelButton"
        Me.outCancelButton.Size = New System.Drawing.Size(142, 50)
        Me.outCancelButton.TabIndex = 8
        Me.outCancelButton.Text = "Cancelar"
        Me.outCancelButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.outCancelButton.UseVisualStyleBackColor = true
        '
        'errorProvider
        '
        Me.errorProvider.ContainerControl = Me
        '
        'DiscountStockForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.ClientSize = New System.Drawing.Size(671, 313)
        Me.Controls.Add(Me.outCancelButton)
        Me.Controls.Add(Me.outProductButton)
        Me.Controls.Add(Me.productGroupBox)
        Me.Controls.Add(Me.emptyLinkLabel)
        Me.Controls.Add(Me.productsGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "DiscountStockForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Descontar Stock"
        CType(Me.productsGrid,System.ComponentModel.ISupportInitialize).EndInit
        Me.productGroupBox.ResumeLayout(false)
        Me.productGroupBox.PerformLayout
        CType(Me.errorProvider,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

        Friend WithEvents emptyLinkLabel As LinkLabel
        Friend WithEvents productsGrid As DataGridView
        Friend WithEvents productGroupBox As GroupBox
        Friend WithEvents quantityTextBox As TextBox
        Friend WithEvents priceTextBox As TextBox
        Friend WithEvents Label3 As Label
        Friend WithEvents Label2 As Label
        Friend WithEvents descriptionTextBox As TextBox
        Friend WithEvents Label1 As Label
        Friend WithEvents outProductButton As Button
        Friend WithEvents outCancelButton As Button
        Friend WithEvents Label4 As Label
        Friend WithEvents discountTextBox As TextBox
        Friend WithEvents ColumnProductName As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductRegistrationDate As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductPrice As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductQuantity As DataGridViewTextBoxColumn
        Friend WithEvents ColumnProductState As DataGridViewTextBoxColumn
        Friend WithEvents errorProvider As ErrorProvider
    End Class
End NameSpace